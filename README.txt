Running locally
========

From the project root folder, execute:

    mvn clean verify
    mvn -Pcargo.run -Drepo.path=storage
    
   Go to ==> http://localhost:8080/site/

